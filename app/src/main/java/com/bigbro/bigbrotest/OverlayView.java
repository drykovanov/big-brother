package com.bigbro.bigbrotest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by drykovanov on 12.06.16.
 */
public class OverlayView extends View {

    public enum State {
        RECT_NOT_SET,
        RECT_SET,
        RECT_FOCUSED,
        RECT_SUSPECT,
        RECT_ALARM
    }

    private Rect rect = null;
    private RectRel track = null;
    private State state = State.RECT_NOT_SET;

    public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (track != null) {

            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);

            // TODO suspect and alarm states
//            if (State.RECT_FOCUSED == state) {
            paint.setColor(0xA000FF00);
//            } else {
//                paint.setColor(0xFF33B5E5);
//            }
            paint.setStrokeWidth(5);

            Rect r = track.toRect(new Rect(0, 0, getWidth(), getHeight()));

            canvas.drawRect(r, paint);
        }

        if (rect != null) {
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);

            // TODO suspect and alarm states
            if (State.RECT_FOCUSED == state) {
                paint.setColor(0xFF00FF00);
            } else {
                paint.setColor(0xFF33B5E5);
            }
            paint.setStrokeWidth(5);

            canvas.drawRect(rect, paint);
        }


    }

    public void setRect(Rect rect) {
        this.rect = rect;
        this.state = State.RECT_SET;
        invalidate();
    }

    public void setTrackRect(RectRel track) {
        this.track = track;
        invalidate();
    }

    public Rect getRect() {
        return this.rect;
    }

    public void setState(State s) {
        this.state = s;
        invalidate();
    }

    public State getState() {
        return state;
    }
}
