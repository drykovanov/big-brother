package com.bigbro.bigbrotest;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaActionSound;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by drykovanov on 12.06.16.
 */
public class PhotoActivity extends AppCompatActivity {

    final static String DEBUG_TAG = "PhotoActivity";

    private View mContentView;
    private TextureView mPreview;
    private OverlayView mOverlay;
    private boolean darkScreen = false;


    private boolean mVisible = true;
    private final Handler mHideHandler = new Handler();

    private Camera mCamera;
    private int cameraId = 0;

    private PreviewCallback mPreviewCallback;

    private MediaActionSound mSound;
    private MediaRecorder mRecorder;
    private File mVideoFile;

    private RectRel mRect = null;
//    private Rect mBounds = null;

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        System.loadLibrary("opencv_core");
        System.loadLibrary("opencv_tracking");
        System.loadLibrary("opencv_java3");
        System.loadLibrary("native");

        // TODO handle load error
        // TODO other eabi?

//        if (!OpenCVLoader.initDebug()) {
//            Log.e(DEBUG_TAG, "Unable to load OpenCV");
//        }

        // SCREEN_ORIENTATION_SENSOR_LANDSCAPE ?
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_photo);

        // prevent sleep
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mContentView = findViewById(R.id.photo_content);
        mPreview = (TextureView) findViewById(R.id.preview);

        mPreview.setSurfaceTextureListener(new MySurfaceTextureListener());

//        SurfaceView mOverlay = (SurfaceView)findViewById(R.id.overlay);
//        mOverlay.setZOrderOnTop(true);    // necessary
//        SurfaceHolder surfaceHld = mOverlay.getHolder();
//        surfaceHld.setFormat(PixelFormat.TRANSLUCENT);
//        mOverlay.setAlpha(0.4f);

        mOverlay = (OverlayView)findViewById(R.id.overlay);

        hide();
        initCamera();
    }

    private void initCamera() {
        // do we have a camera?
        if (!getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, "No camera on this device", Toast.LENGTH_LONG)
                    .show();
        } else {
            cameraId = findBackCamera();
            if (cameraId < 0) {
                Toast.makeText(this, "No back facing camera found.",
                        Toast.LENGTH_LONG).show();
            } else {
                mCamera = Camera.open(cameraId);

                setCameraDisplayOrientation(this, cameraId, mCamera);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            mSound = new MediaActionSound();
            mSound.load(MediaActionSound.FOCUS_COMPLETE);
        }

        if (mPreviewCallback == null) {
            mPreviewCallback = new PreviewCallback(this.mOverlay, this, R.raw.caralarm1);
        }
    }

    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    private void initMediaRecorder() {
        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
//            mRecorder.release();
//            mRecorder = null;
        } else {
            // mRecorder.stop();
            mRecorder.reset();
        }

        Camera.Parameters param = mCamera.getParameters();
        Camera.Size size = param.getPreviewSize();
//        for ( Camera.Size s : param.getSupportedPreviewSizes()) {
//            if (s.width <= 640) {
//                size = s;
//                break;
//            }
//        }

        // no effect
//        param.setColorEffect(Camera.Parameters.EFFECT_MONO);
//        mCamera.setParameters(param);

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mRecorder.setCamera(mCamera);

        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
//        mRecorder.setCamera(mCamera);
        // TODO no sound?
        // 06-14 02:01:27.483: E/MediaRecorder(16057): audio source is set, but audio encoder is not set
//        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);


        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);

        mRecorder.setVideoEncodingBitRate(3000 * 1024); // битрейт видео
        // given video encoder 4 is not found VP8
        mRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);

        // TODO управление файлами, ротация
        // TODO окончание записи
        mRecorder.setOnInfoListener(new MediaRecorderListener());

        // 1 fps -> 24 fps: 2500ms = 1 минута реального времени
//        mRecorder.setMaxDuration(2500 * 60 * 9); // максимальная длительность записи
//        mRecorder.setMaxFileSize(0); // максимальный размер файла

        mRecorder.setCaptureRate(2); // 1
        mRecorder.setVideoFrameRate(24); // фреймрейт записи видео

        mRecorder.setVideoSize(size.width, size.height); // размер картинки

        File sdDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File bbDir = new File(sdDir, PhotoHandler.DIR); // TODO branding
        if (!bbDir.exists()) {
            bbDir.mkdirs();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(PhotoHandler.FILE_DATE_FORMAT);
        String date = dateFormat.format(new Date());
        mVideoFile = new File(bbDir, "" + date + ".3gp");

        mRecorder.setOutputFile(mVideoFile.getAbsolutePath());

        // If setCamera(Camera) is used and the surface
        // has been already set to the camera, application do not need to call this.
        // mRecorder.setPreviewDisplay(Surface);

        try {
            mRecorder.prepare();
        } catch (IOException ioe) {
            Log.d(DEBUG_TAG, "Failed to setup video recording: " + ioe);
            mRecorder.release();
            mRecorder = null;
        }


        if (mRecorder != null) {
            mRecorder.start();

//            mCamera.setOneShotPreviewCallback(new Camera.PreviewCallback() {
//                @Override
//                public void onPreviewFrame(byte[] data, Camera camera) {
//                     mCamera.setOneShotPreviewCallback(this);
//                }
//            });

//            try {
//                mCamera.reconnect();
//                mCamera.setPreviewCallback(new PreviewCallback());
//            } catch (Exception e) {
//                Log.e(DEBUG_TAG, "Failed to reconnect to camera");
//            }
            // TODO
//            после окончания записи:
//            — запрещаем совместный доступ к камере
//            camera.reconnect();
//
//            — включаем превью камеры
//            mCamera.startPreview();


        }
    }

    private class MediaRecorderListener implements MediaRecorder.OnInfoListener {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            Log.d(DEBUG_TAG, "onInfo()");
            if (MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED == what) {
                mr.stop();
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     *
     * @param rect - region of intereset (initial car bounds)
     * @param bounds - preview size
     */
    private void setRect(Rect rect, Rect bounds) {
        if (rect != null) {
            this.mRect = new RectRel(rect, bounds);

            if (!rect.equals(this.mRect.toRect(bounds))) {
                Log.w(DEBUG_TAG, "Error in RectRel");
            }
        } else {
            this.mRect = null;
        }

        if (mOverlay != null) {
            mOverlay.setRect(rect);
        }
    }

    @Override
    protected void onPause() {
//        if (mOverlay != null) {
//            mOverlay.setRect(null);
//        }
        setRect(null, null);

        if (mRecorder != null) {
            try {
                mRecorder.stop();  // stop the recording
            } catch (RuntimeException e) {
                Log.e(DEBUG_TAG, "MediaRecorder already stopped: " + e);
            }
            mRecorder = null;

            try {
                if (mCamera != null) {
                    mCamera.lock();
                }
            } catch (RuntimeException e) {
                // RuntimeException is thrown when stop() is called immediately after start().
                // In this case the output file is not properly constructed ans should be deleted.
                Log.d(DEBUG_TAG, "RuntimeException: stop() is called immediately after start()");
                //noinspection ResultOfMethodCallIgnored
               // mOutputFile.delete();
            }
        }

        if (mCamera != null) {
            mCamera.stopPreview();

            mCamera.setPreviewCallbackWithBuffer(null);
            try {
                mCamera.setPreviewTexture(null);
            } catch (IOException ioe) {
                Log.e(DEBUG_TAG, "Failed to stop camera preview: " + ioe);
            }
            mCamera.release();
            mCamera = null;
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mCamera == null) {
            initCamera();
        }

        // default brightness
        setDarkScreen(false);

        super.onResume();
    }

    @Override
    protected void onStop() {
        if (mRecorder != null) {
            mRecorder.release();
        }

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private int findBackCamera() {
        int cameraId = -1;
        // Search for the back facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Log.d(DEBUG_TAG, "Camera found");
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.postDelayed(mHidePart2Runnable, 200);
    }



    private class MySurfaceTextureListener implements TextureView.SurfaceTextureListener {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            if (mCamera == null) {
                initCamera();
            }

            Camera.Parameters params = mCamera.getParameters();
            Camera.Size previewSize = params.getPreviewSize();
            for ( Camera.Size s : params.getSupportedPreviewSizes()) {
                if (s.width <= 640) {
                    previewSize = s;
                    break;
                }
            }
            params.setPreviewSize(previewSize.width, previewSize.height);


//            List<Integer> formats = params.getSupportedPreviewFormats();
            // It is strongly recommended that either NV21 or YV12 is used,
            //  since they are supported by all camera devices.
//            params.setPreviewFormat(ImageFormat.NV21);

            try {
                mCamera.setPreviewTexture(surface);
                mCamera.setPreviewCallback(mPreviewCallback);
            } catch (IOException t) {
                Log.e(DEBUG_TAG, "Failed to start preview: " + t);
            }
            mCamera.startPreview();

            if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)) {
                mCamera.autoFocus(new AutoFocusCallback());
            }

//            mPreview.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)) {
//                        mCamera.autoFocus(new MyAutoFocusCallback());
//                    }
//                }
//            });

            mPreview.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        handleTouchMove(v, event);
                    } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        handleTouchStart(v, event);
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        handleTouchDone(v, event);
                    }

                    return true;
                }
            });


            Camera.Parameters param = mCamera.getParameters();

            param.setRecordingHint(false);
            param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);

            if (param.getSupportedWhiteBalance().contains(Camera.Parameters.WHITE_BALANCE_AUTO)) {
                param.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
            }

            if (param.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            } else if (param.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }

            if(param.isZoomSupported()) {
                param.setZoom(0);
            }

//            int fps[] = new int[2];
//            params.getPreviewFpsRange(fps);

//            param.setColorEffect(Camera.Parameters.EFFECT_MONO);

            mCamera.setParameters(param);

//            initMediaRecorder();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    }

    private void setDarkScreen(boolean dark) {
        this.setDarkScreen(dark, true);
    }
    private void setDarkScreen(boolean dark, boolean change) {
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.screenBrightness = (dark ? 0.0f : -1.0f);
        getWindow().setAttributes(params);

        if (change) {
            this.darkScreen = dark;
        }
    }

    private List<Point> dragPoints = new LinkedList<>();

    private void handleTouchStart(View v, MotionEvent event) {
        if (darkScreen) {
            // default brightness
            setDarkScreen(false, false);
        } else {
            dragPoints.clear();
            handleTouchMove(v, event);
        }
    }


    private void handleTouchDone(View v, MotionEvent event) {
        if (darkScreen) {
            // default brightness
            setDarkScreen(false);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    setDarkScreen(true);
                }
            }, 5 * 1000);


        } else {
            mCamera.cancelAutoFocus();
            mCamera.autoFocus(new AutoFocusCallback());
        }
    }

    private void handleTouchMove(View v, MotionEvent event) {
        if (darkScreen) {
            return ;
        }

        // TODO relative to View
        dragPoints.add(new Point((int)event.getX(), (int)event.getY()));
        Rect rect = toRect(dragPoints);

        setRect(rect, new Rect(0, 0, mPreview.getWidth(), mPreview.getHeight()));

        final Camera.Area area = toArea(rect);
        List<Camera.Area> areas = new ArrayList<Camera.Area>(1);
        areas.add(area);

        Camera.Parameters params = mCamera.getParameters();
        if (params.getMaxNumFocusAreas() >= 1) {
            params.setFocusAreas(areas);
        }

        if (params.getMaxNumMeteringAreas() >= 1) {
            params.setMeteringAreas(areas);
        }

        if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }

        mCamera.setParameters(params);
    }


    int normToArea(int x, int maxx) {
        int n = (int)((x - maxx/2) * (2000f / maxx));

        if (n > 1000)
            n = 1000;
        if (n < -1000)
            n = -1000;

        return n;
    }

    private Camera.Area toArea(Rect rect) {
        int w = mPreview.getWidth();
        int h = mPreview.getHeight();

        return new Camera.Area(new Rect(
                normToArea(rect.left, w),
                normToArea(rect.top, h),
                normToArea(rect.right, w),
                normToArea(rect.bottom, h)
        ), 1000);
    }

    private static Rect toRect(List<Point> points) {
        if (points.isEmpty())
            return null;

        Point p0 = points.get(0);
        int minX = p0.x;
        int minY = p0.y;
        int maxX = p0.x;
        int maxY = p0.y;

        for (Point p : points) {
            int x = p.x;
            int y = p.y;

            if (x > maxX) {
                maxX  = x;
            } else if (x < minX) {
                minX = x;
            }

            if (y > maxY) {
                maxY  = y;
            } else if (y < minY) {
                minY = y;
            }
        }

        if (Math.abs(maxX - minX) < 100.0) {
            int mid = (maxX + minX) / 2;
            minX = mid - 50;
            maxX = mid + 50;
        }

        if (Math.abs(maxY - minY) < 100.0) {
            int mid = (maxY + minY) / 2;
            minY = mid - 50;
            maxY = mid + 50;
        }

        return new Rect((int)minX, (int)minY, (int)maxX, (int)maxY);
    }

    class AutoFocusCallback implements Camera.AutoFocusCallback {

//        private final Camera mCamera;
//        private final MediaActionSound mSound;
//
//        AutoFocusCallback(Camera mCamera, MediaActionSound mSound) {
//            this.mCamera = mCamera;
//            this.mSound = mSound;
//        }

        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            if (mPreviewCallback != null) {
                mPreviewCallback.setRect(mRect);
            }

            if (success) {
                mOverlay.setState(OverlayView.State.RECT_FOCUSED);

                // TODO изменить на звук установленной сигнализации
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    mSound.play(MediaActionSound.FOCUS_COMPLETE);
                }

                Camera.Parameters param = mCamera.getParameters();

                if (param.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_FIXED)) {
                    param.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
                }

                mCamera.setParameters(param);

                // min brightness
//                setDarkScreen(true);

                // test takePicture
                mCamera.takePicture(
                    null,
                    null,
                    null,
                    new PhotoHandler(PhotoActivity.this)
                );
            } else {
                Camera.Parameters param = mCamera.getParameters();

                if (param.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                    param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                } else if (param.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                    param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                }

                mCamera.setParameters(param);
            }
        }
    }
}
