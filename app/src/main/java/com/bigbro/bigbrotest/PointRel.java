package com.bigbro.bigbrotest;

/**
 * Created by drykovanov on 23.10.16.
 */
public class PointRel {

    public final float x0; // public for easier jni access
    public final float y0;

    public PointRel(float x0, float y0) {
        this.x0 = x0;
        this.y0 = y0;
    }

    public float getX0() {
        return x0;
    }

    public float getY0() {
        return y0;
    }

    public float distance(PointRel p1) {

        final float dx = x0 - p1.x0;
        final float dy = y0 - p1.y0;

        return (float)Math.sqrt(dx * dx + dy * dy);
    }

}
