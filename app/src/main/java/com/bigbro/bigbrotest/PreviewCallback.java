package com.bigbro.bigbrotest;

import android.app.Activity;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.provider.Settings;
import android.util.Log;

import com.bigbro.bigbrotest.alarm.RoiAlarmManager;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by drykovanov on 14.06.16.
 */
public class PreviewCallback implements Camera.PreviewCallback {

    final static String DEBUG_TAG = "PreviewCallback";


    private final OverlayView overlay;

    private RectRel mRect = null;
    private boolean trackerInited = false;
    private final RoiAlarmManager alarm;


    public PreviewCallback(OverlayView overlay, Activity activity, int caralarm) {
        this.overlay = overlay;
        this.alarm = new RoiAlarmManager(activity, caralarm);
    }

    public void setRect(RectRel rect) {
        this.mRect = rect;
        this.trackerInited = false;
        this.ts0 = -1;
    }

    private long ts0 = -1;

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (mRect != null) {
            Camera.Size size = camera.getParameters().getPreviewSize();

            int previewFormat = camera.getParameters().getPreviewFormat();
            if (ImageFormat.NV21 != previewFormat) {
                Log.e(DEBUG_TAG, "Unsupported image format: " + previewFormat);
                return ;
            }

            Mat m = new Mat(size.height + size.height / 2, size.width, CvType.CV_8UC1);
            m.put(0, 0, data);

            float ratio = ((float)m.cols()) / m.rows();
            int h = 360;
            int w = (int)(ratio * h);
//            int w = 600;
//            int h = (int)(w / ratio);

            Mat resized = new Mat();
            Imgproc.resize(m, resized, new Size(w, h));
            m = resized;

            Mat matRgb = new Mat();
            Imgproc.cvtColor(m, matRgb, Imgproc.COLOR_YUV420sp2BGR);

            if (!trackerInited) {
                boolean inited = initTracker(matRgb.nativeObj,
                        mRect.x0, mRect.x1, mRect.y0, mRect.y1);
                trackerInited = inited;
                alarm.startRoi(mRect);
            } else {
                long ts1 = System.currentTimeMillis();
//                if (ts0 < 0 || (ts1 - ts0) > (1000 / 2))
                {
                    RectRel roiTracked = track(matRgb.nativeObj);

                    ts0 = System.currentTimeMillis();
                    Log.d(DEBUG_TAG, "track(): " + (ts0 - ts1) + "ms");

                    alarm.updateRoi(roiTracked);

                    overlay.setTrackRect(roiTracked);
                }
            }
        }
    }

    private native boolean initTracker(
            long mat_nativeObj,
            float x0, float x1, float y0, float y1);

    private native RectRel track(
            long mat_nativeObj);
}