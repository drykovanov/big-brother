package com.bigbro.bigbrotest;

/**
 * Created by drykovanov on 13.07.16.
 */
public class RectRel {

    public final float x0; // public for easier jni access
    public final float x1;
    public final float y0;
    public final float y1;

    public RectRel(float x0, float x1, float y0, float y1) {
        this.x0 = x0;
        this.x1 = x1;
        this.y0 = y0;
        this.y1 = y1;
    }

    public RectRel(android.graphics.Rect rect, android.graphics.Rect bounds) {
        x0  = ((float)rect.left) / bounds.width();
        x1  = ((float)rect.right) / bounds.width();
        y0  = ((float)rect.top) / bounds.height();
        y1  = ((float)rect.bottom) / bounds.height();
    }

    public float getX0() {
        return x0;
    }

    public float getX1() {
        return x1;
    }

    public float getY0() {
        return y0;
    }

    public float getY1() {
        return y1;
    }

    public android.graphics.Rect toRect(android.graphics.Rect bounds) {
        return new android.graphics.Rect(
                (int)(x0 * bounds.width()),
                (int)(y0 * bounds.height()),
                (int)(x1 * bounds.width()),
                (int)(y1 * bounds.height())
        );
    }

    @Override
    public String toString() {
        return "["  + x0 + "," + y0 + "," + x1 + "," + y1 + "]";
    }

    public PointRel center() {
        return new PointRel((x0 + x1) / 2, (y0 + y1) / 2);
    }

    public float area() {
        return Math.abs(x1 - x0) * Math.abs(y1 - y0);
    }

    public float width() {
        return Math.abs(x0 - x1);
    }

    public float height() {
        return Math.abs(y0 - y1);
    }
}
