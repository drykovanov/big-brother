package com.bigbro.bigbrotest.alarm;

import android.app.Activity;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import com.bigbro.bigbrotest.PointRel;
import com.bigbro.bigbrotest.RectRel;

/**
 * Created by drykovanov on 23.10.16.
 */
public class RoiAlarmManager {

    final static String DEBUG_TAG = "RoiAlarmManager";

    private RectRel initialRoi = null;
    private final SoundPool soundPool;
    private final int soundID;

    public RoiAlarmManager(Activity activity, int caralarm) {
        this.soundPool = new SoundPool(1, AudioManager.STREAM_ALARM, 0);
        this.soundID = this.soundPool.load(activity, caralarm, 1);
        // TODO load listener, what if failed?
    }

    public void startRoi(RectRel roi) {
        this.initialRoi = roi;
    }

    private boolean isAlarmed = false;
    private int streamID = -1;

    public void updateRoi(RectRel tracked) {
        if (tracked != null) {

            float a0 = initialRoi.area();
            float a1 = tracked.area();

            PointRel c0 = initialRoi.center();
            PointRel c1 = tracked.center();

            float d = c0.distance(c1);
            float dx = Math.abs(c0.x0 - c1.x0);
            float dy = Math.abs(c0.y0 - c1.y0);

            Log.d(DEBUG_TAG, "Area: " + a0 + " -> " + a1);
            Log.d(DEBUG_TAG, "Center move: " + d + ", dx=" + dx + ", dy="+dy);


            if ((dx > initialRoi.width() / 2)
                || (dy > initialRoi.height() / 2)) {

                Log.w(DEBUG_TAG, "Alarm!!!");

                if (!isAlarmed) {
                    isAlarmed = true;

                    streamID = soundPool.play(soundID,
                            0.1f, 0.1f, // volume
                            1,           // priority
                            -1,         // loop
                            1.0f);      // rate
                }
            } else {
                if (isAlarmed) {
                    isAlarmed = false;

                    if (streamID != -1) {
                        soundPool.stop(streamID);
                    }
                }
            }

        } else {
            // TODO
        }
    }
 }
