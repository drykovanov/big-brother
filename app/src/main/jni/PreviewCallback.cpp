//
// Created by drykovanov on 09.07.16.
//
#include <jni.h>

#include <android/log.h>

#include <opencv2/core/utility.hpp>
#include <opencv2/core.hpp>
#include <opencv2/tracking.hpp>

#define  LOG_TAG    "PreviewCallback"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

using namespace std;

cv::Ptr<cv::Tracker> tracker;
cv::Rect2d roi;

extern "C"
JNIEXPORT jboolean JNICALL Java_com_bigbro_bigbrotest_PreviewCallback_initTracker(
    JNIEnv *env, jobject thiz,
    jlong mat_nativeObj,
    jfloat x0, jfloat x1, jfloat y0, jfloat y1 // roi
    ) {

    cv::Mat& image = *((cv::Mat *)mat_nativeObj);

    float ratio = ((float)image.cols) / image.rows;
    ALOG("Image image: %d x %d, %.2f", image.cols, image.rows, ratio);

    int ix0 = (int)(x0 * image.cols);
    int ix1 = (int)(x1 * image.cols);
    int iy0 = (int)(y0 * image.rows);
    int iy1 = (int)(y1 * image.rows);

    ALOG("roi: [%d,%d - %d,%d]", ix0, iy0, ix1, iy1);

    ALOG("before tracker init");
    roi = cv::Rect2d(ix0, iy0, (ix1 - ix0), (iy1 - iy0));
    tracker = cv::Tracker::create("KCF");
    bool inited = tracker->init(image, roi);
    ALOG("tracker inited: %d", inited);
    ALOG("roi: [%.2f,%.2f]", roi.x, roi.y);

    return inited;
}

extern "C"
JNIEXPORT jobject JNICALL Java_com_bigbro_bigbrotest_PreviewCallback_track(
        JNIEnv *env, jobject thiz,
        jlong mat_nativeObj) {

    cv::Mat& image = *((cv::Mat *)mat_nativeObj);
    bool updated = tracker->update(image, roi);
    ALOG("updated: %d, roi: [%.3f,%.3f]", updated,
         (roi.x + (roi.width /2)) / image.cols,
         (roi.y + (roi.height / 2)) / image.rows);

    if (!updated) {
        return 0;
    } else {
        jclass c = env->FindClass("com/bigbro/bigbrotest/RectRel");
        if (c == 0) {
            ALOG("find class failed");
        }
        jmethodID ctor = env->GetMethodID(c, "<init>", "(FFFF)V");
        if (ctor == 0) {
            ALOG("find ctor failed");
        }


        jobject rectRel = env->NewObject(c, ctor,
             (float)(roi.x  / image.cols),
             (float)((roi.x + roi.width)  / image.cols),
             (float)(roi.y  / image.rows),
             (float)((roi.y + roi.height)  / image.rows));

        return rectRel;
    }
}